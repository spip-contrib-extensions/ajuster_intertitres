# CHANGELOG

## 1.1.3 - 2024-05-07

### Fixed

- Compatible SPIP 4.y.z

## 1.1.2 - 2023-02-25

### Fixed

- Compatible SPIP 4.2
