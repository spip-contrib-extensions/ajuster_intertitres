# Ajuster intertitres

## Documentation
https://contrib.spip.net/5344

## Exemple pour dev

````
<poesie>
<h2>Intertitre dans poésie (h2 - stable)</h2>
</poesie>

<code class="html5">
<h2>Intertitre dans code (h2 - stable)</h2>
</code>

<cadre class="html5">
<h2>Intertitre dans code (h2 - stable)</h2>
</cadre>


<quote>
<h2>Intertitre dans citation (h2 - stable)</h2>
</quote>


<h2>Intertitre standard (h2 --> h3)</h2>
````
