<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[ $GLOBALS['idx_lang'] ] = array(

	// C
	'paulbloas_description' => '',
	'paulbloas_nom'         => 'Ajuster intertitres',
	'paulbloas_slogan'      => 'Un filtre |ajuster_intertitres pour ajuster les intertitres dans le contenu, par exemple dans les maquettes type one-page',
);
